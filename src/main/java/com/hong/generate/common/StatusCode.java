package com.hong.generate.common;

/**
 * @description:
 * @author: YJH
 * @time: 2020/4/5 11:52
 */
public class StatusCode {

    public static final int SUCCESS = 200;
    public static final int ERROR = 500;
}
